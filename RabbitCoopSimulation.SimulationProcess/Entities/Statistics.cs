﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace RabbitCoopSimulation.SimulationProcess.Entities
{
    public class Statistics
    {
        public int Malelifetime { get; set; }
        public int Femalelifetime { get; set; }
        public int TimeofPregnancy { get; set; }
        public int LoseofFertility { get; set; }
        public int PercentageofRabbitsBorn { get; set; }

        public Statistics()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            Malelifetime = Convert.ToInt32(configuration["malelifetime"]);
            Femalelifetime = Convert.ToInt32(configuration["femaleliftetime"]);
            TimeofPregnancy = Convert.ToInt32(configuration["timeofPregnancy"]);
            LoseofFertility = Convert.ToInt32(configuration["loseofFertality"]);
            PercentageofRabbitsBorn = Convert.ToInt32(configuration["percentageofRabbitsBorn"]);
        }
    }
}
