﻿using System;
using System.Dynamic;
using System.IO;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitCoopSimulation.SimulationProcess.Entities.Abstract;
using RabbitCoopSimulation.SimulationProcess.Entities.Concrete;

namespace RabbitCoopSimulation.SimulationProcess
{
    class Program
    {
        public static ICoop _coop;

        public Program(ICoop coop)
        {
            _coop = coop;
        }

        static void Main(string[] args)
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            ConfigureService(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var service = serviceProvider.GetService<Program>();

            BeginSimulate();
        }

        private static void ConfigureService(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICoop, Coop>();
            serviceCollection.AddTransient<Program>();
        }

        static void BeginSimulate()
        {
            Console.WriteLine("Simulation length(month): ");
            int months = Convert.ToInt32(Console.ReadLine());

            _coop.AddAnimal(new MaleRabbit());
            _coop.AddAnimal(new FemaleRabbit());
            
            for (int month = 0; month <= months; month++)
            {
                foreach (var femaleRabbit in  _coop.GetFemaleAnimals())
                {
                    if (femaleRabbit.LoseofFertility >= femaleRabbit.Age
                        && femaleRabbit.Age >= 0
                        && !femaleRabbit.IsPregnant
                        && _coop.GetMaleAnimalCount() > 0)
                    {
                        femaleRabbit.Act();
                    }
                }

                _coop.PopulationCyle();
                _coop.PopulationCyleForFemaleAnimals();

                Console.WriteLine(month + ". Month: " +
                                  "Male Rabbit Count "
                                  + _coop.GetMaleAnimalCount()
                                  + " "
                                  + "Female Rabbit Count " +
                                  +_coop.GetFemaleAnimalCount());
            }

            Console.ReadKey();
        }
    }
}
