# Rabbit Coop Simulation
> At the end of the simulation gives number of male / female rabbits count


The project is a rabbit population simulation.

## Class Diagrams

### Animal



![](images/AnimalDiagram.png)

## Class Attiributes, Methods

### MaleRabbit 

| Attribute | Type |        Description                 |
| --------- | ---- |----------------------------        |
| GenderType| int  | Contains rabbit's gender           |
| LifeTime  | int  | Contains rabbit's lifetime (month) |
| Age       | int  | Contains rabbit's age (month)      |

#### Act

| Input     | Output |        Description                                                    |
| --------- | ----   |-----------------------------------------------------------------------|
| void      | void   | Methods execute when male rabbit's sexual act                         |



### FemaleRabbit 

| Attribute         | Type  |                           Description                        |
| ------------------| ----- |--------------------------------------------------------------|
| LoseofFertility   | int   | Contains rabbit's lose Of Fertility period (month)           |
| TimeofPregnancy   | int   | Contains rabbit's pregnancy period if the rabbit has pregnant|
| IsPregnant        | bool  | Contains rabbit's pregnancy state                            |

#### Methods

| Name | Input     | Output        |        Description                                                    |
| ---- | --------- | --------------|-----------------------------------------------------------------------|
| Act  | void      | void          | Method execute when female rabbit's sexual act                        |
| Breed| void      | List<Animal>  | Method execute when female rabbit's breed new one                     |


### Coop

![](images/coopDiagram.png)


## Class Attiributes, Methods

### Coop 

| Attribute | Type           |        Description         |
| --------- | ----           |----------------------------|
| AnimalList| List<IAnimal>  | Contains animal's          |


#### Methods

| Name                                | Input           | Output                |        Description                                                    |
| ----                                | ---------       | ----                  |-----------------------------------------------------------------------|
| AddAnimal                           | IAnimal         | void                  | Method add a new animal                                               |
| AddAnimals                          | List<IAnimal>   | void                  | Method add a new animals                                              |
| RemoveAnimal                        | IAnimal         | void                  | Method remove current animal in the coop                              |
| GetMaleAnimalCount                  | IAnimal         | void                  | Method returns male animal count                                      |
| GetFemaleAnimalCount                | IAnimal         | void                  | Method returns female animal count                                    |
| GetFemaleAnimals                    | IAnimal         | List<IFemaleAnimal>   | Method returns female animals                                         |
| GetCount                            | void            | int                   | Method returns animals                                                |
| PopulationCyle                      | void            | void                  | Method execute population cyle for a month                            |
| PopulationCyleForFemaleAnimals      | void            | void                  | Method execute population cyle for a month only female animals        |



## Class Diagrams

Simulation works according to these paramters:

**Note:**
Parameter values store at appSettings.json file.

| Nane            |                         Description                            |
| ---------       | ---------------------------------------------------------------|
| malelifetime    |  Contains Male   Rabbits lifetime                              |
| femaleliftetime |  Contains Female Rabbits lifetime                              |
| timeofPregnancy |  Contains rabbit's pregnancy period if the rabbit has pregnant |
| loseofFertality |  Contains rabbit's lose Of Fertility period (month)            |


## Benchmark Results (for 100 month)

###Hardware Specs.

Intel Xeon CPU E5-2620 @2.00 GHz
16 GB RAM

![](images/benchmark.png)

