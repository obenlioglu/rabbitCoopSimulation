﻿namespace RabbitCoopSimulation.SimulationProcess.Entities.Concrete
{
    public enum Gender
    {
        Female,
        Male
    }
}