﻿using System.Collections.Generic;

namespace RabbitCoopSimulation.SimulationProcess.Entities.Abstract
{
    public interface ICoop
    {

        //props
        List<IAnimal> AnimalList { get; set; }

        //methods

        void AddAnimal(IAnimal animal);
        void AddAnimals(List<IAnimal> animals);

        void RemoveAnimal(IAnimal animal);
        
        int GetMaleAnimalCount();
        int GetFemaleAnimalCount();

        List<IFemaleAnimal> GetFemaleAnimals();

        //kümesteki hayvan sayısı
        int GetCount();
        void PopulationCyle();
        void PopulationCyleForFemaleAnimals();
        //void Act(IFemaleAnimal femaleRabbit);
    }
}