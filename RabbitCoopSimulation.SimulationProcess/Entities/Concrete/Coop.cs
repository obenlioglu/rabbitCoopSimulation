﻿using System.Collections.Generic;
using System.Linq;
using RabbitCoopSimulation.SimulationProcess.Entities.Abstract;

namespace RabbitCoopSimulation.SimulationProcess.Entities.Concrete
{
    public class Coop: ICoop
    {
        public List<IAnimal> AnimalList { get; set; }

        public Coop()
        {
            AnimalList = new List<IAnimal>();
        }
        
        public void AddAnimal(IAnimal animal)
        {
            AnimalList.Add(animal);
        }

        public void AddAnimals(List<IAnimal> animals)
        {
            AnimalList.AddRange(animals);
        }

        public void RemoveAnimal(IAnimal animal)
        {
            AnimalList.Remove(animal);
        }

        public int GetCount()
        {
            return AnimalList.Count;
        }

        public void PopulationCyle()
        {
            foreach (var animal in AnimalList.ToList())
            {
                animal.Age++;

                if (animal.Age >= animal.Lifetime)
                {
                    RemoveAnimal(animal);
                }
            }
        }
        
        public void PopulationCyleForFemaleAnimals()
        {
            foreach (var femaleAnimal in GetFemaleAnimals())
            {
                if (femaleAnimal.IsPregnant)
                {
                    if (femaleAnimal.TimeofPregnancy == 0)
                    {
                        AddAnimals(femaleAnimal.Breed());
                        femaleAnimal.IsPregnant = false;
                    }
                    femaleAnimal.TimeofPregnancy--;
                }
            }
        }
        
        public int GetMaleAnimalCount()
        {
            return AnimalList.Count(x => x.GenderType == (int) Gender.Male);
        }

        public int GetFemaleAnimalCount()
        {
            return AnimalList.Count(x => x.GenderType == (int)Gender.Female);
        }

        public List<IFemaleAnimal> GetFemaleAnimals()
        {
            List<IFemaleAnimal> femaleAnimals = AnimalList.Where(x => x.GenderType == (int)Gender.Female).Cast<IFemaleAnimal>().ToList();
            
            return femaleAnimals.ToList();
        }

        public List<IMaleAnimal> GetMaleAnimals()
        {
            List<IMaleAnimal> maleAnimals = AnimalList.Where(x => x.GenderType == (int)Gender.Male).Cast<IMaleAnimal>().ToList();

            return maleAnimals.ToList();
        }

        public List<IAnimal> GetAnimals()
        {
            return AnimalList;
        }
    }
}