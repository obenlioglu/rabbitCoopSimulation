﻿using RabbitCoopSimulation.SimulationProcess.Entities.Abstract;

namespace RabbitCoopSimulation.SimulationProcess.Entities.Concrete
{
    public class MaleRabbit: IRabbit, IMaleAnimal
    {
        public int GenderType { get; set; }
        public int Lifetime { get; set; }
        public int Age { get; set; }
        

        public MaleRabbit()
        {
            Statistics statistic = new Statistics();

            GenderType = (int)Gender.Male;
            Age = 0;
            Lifetime = statistic.Malelifetime;
        }

        public void Act()
        {
            //MaleRabbit acts with the female one.
        }
    }
}