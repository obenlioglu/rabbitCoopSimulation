﻿namespace RabbitCoopSimulation.SimulationProcess.Entities.Abstract
{
    public interface IAnimal
    {
        int GenderType { get; set; }
        int Lifetime { get; set; }
        int Age { get; set; }

        void Act();
    }
}