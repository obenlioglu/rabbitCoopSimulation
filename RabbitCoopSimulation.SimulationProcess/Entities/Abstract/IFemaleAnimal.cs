﻿using System.Collections.Generic;

namespace RabbitCoopSimulation.SimulationProcess.Entities.Abstract
{
    public interface IFemaleAnimal : IAnimal
    {
        int LoseofFertility { get; set; }
        int TimeofPregnancy { get; set; }
        bool IsPregnant { get; set; }

        List<IAnimal> Breed();
    }
}