﻿using System;
using System.Collections.Generic;
using RabbitCoopSimulation.SimulationProcess.Entities.Abstract;

namespace RabbitCoopSimulation.SimulationProcess.Entities.Concrete
{
    public class FemaleRabbit: IRabbit, IFemaleAnimal
    {
        public int GenderType { get; set; }
        public int Lifetime { get; set; }
        public int Age { get; set; }
        

        public int LoseofFertility { get; set; }
        public int TimeofPregnancy { get; set; }
        public bool IsPregnant { get; set; }
        
        public FemaleRabbit()
        {
            Statistics statistic = new Statistics();

            GenderType = (int) Gender.Female;
            Age = 0;
            Lifetime = statistic.Femalelifetime;
            LoseofFertility = statistic.LoseofFertility;
            TimeofPregnancy = statistic.TimeofPregnancy;
        }

        public void Act()
        {
            IsPregnant = true;
        }

        public List<IAnimal> Breed()
        {
            List<IAnimal> childRabbits = new List<IAnimal>();

            Random random = new Random();

            int childCount = random.Next(3, 5);

            for (int i = 0; i < childCount; i++)
            {
                int genderType = random.Next(0, 2);

                switch (genderType)
                {
                    case (int)Gender.Female:
                        childRabbits.Add(new FemaleRabbit());
                        break;
                    case (int)Gender.Male:
                        childRabbits.Add(new MaleRabbit());
                        break;
                }
            }
            return childRabbits;
        }
        
    }
}